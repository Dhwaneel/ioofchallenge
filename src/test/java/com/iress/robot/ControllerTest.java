package com.iress.robot;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Controller test.
 *
 * @author dhwaneel
 */
public class ControllerTest {

  private Controller appUnderTest;
  private Robot robot;
  private int tableWidth;
  private int tableHeight;

  @Before
  public void setUp() {
    appUnderTest = new Controller();
    robot = appUnderTest.getRobot();
    tableWidth = appUnderTest.getTable().getWidth();
    tableHeight = appUnderTest.getTable().getHeight();
  }

  @Test
  public void test_placeRobot() {
    appUnderTest.placeRobot("-1,1,F");
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());

    appUnderTest.placeRobot("1,-1,F");
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());

    appUnderTest.placeRobot("6,6,F");
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());

    appUnderTest.placeRobot("5,6,F");
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());

    appUnderTest.placeRobot("6,5,F");
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());

    appUnderTest.placeRobot("0,0,NORTH");
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());
    assertEquals(Face.NORTH, robot.getFace());

    appUnderTest.placeRobot("5,5,NORTH");
    assertEquals(5, robot.getX());
    assertEquals(5, robot.getY());
    assertEquals(Face.NORTH, robot.getFace());

    appUnderTest.placeRobot("1,3,NORTH");
    assertEquals(1, robot.getX());
    assertEquals(3, robot.getY());
    assertEquals(Face.NORTH, robot.getFace());
  }

  @Test
  public void test_moveRobot_origin() {
    robot.setX(0);
    robot.setY(0);
    robot.setFace(Face.WEST);
    appUnderTest.moveRobot();
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());

    robot.setX(0);
    robot.setY(0);
    robot.setFace(Face.NORTH);
    appUnderTest.moveRobot();
    assertEquals(0, robot.getX());
    assertEquals(1, robot.getY());

    robot.setX(0);
    robot.setY(0);
    robot.setFace(Face.EAST);
    appUnderTest.moveRobot();
    assertEquals(1, robot.getX());
    assertEquals(0, robot.getY());

    robot.setX(0);
    robot.setY(0);
    robot.setFace(Face.SOUTH);
    appUnderTest.moveRobot();
    assertEquals(0, robot.getX());
    assertEquals(0, robot.getY());
  }

  @Test
  public void test_moveRobot_topLeftCorner() {
    robot.setX(0);
    robot.setY(tableHeight);
    robot.setFace(Face.WEST);
    appUnderTest.moveRobot();
    assertEquals(0, robot.getX());
    assertEquals(tableHeight, robot.getY());

    robot.setX(0);
    robot.setY(tableHeight);
    robot.setFace(Face.NORTH);
    appUnderTest.moveRobot();
    assertEquals(0, robot.getX());
    assertEquals(tableHeight, robot.getY());

    robot.setX(0);
    robot.setY(tableHeight);
    robot.setFace(Face.EAST);
    appUnderTest.moveRobot();
    assertEquals(1, robot.getX());
    assertEquals(tableHeight, robot.getY());

    robot.setX(0);
    robot.setY(tableHeight);
    robot.setFace(Face.SOUTH);
    appUnderTest.moveRobot();
    assertEquals(0, robot.getX());
    assertEquals(tableHeight - 1, robot.getY());
  }

  @Test
  public void test_moveRobot_topRightCorner() {
    robot.setX(tableWidth);
    robot.setY(tableHeight);
    robot.setFace(Face.WEST);
    appUnderTest.moveRobot();
    assertEquals(tableWidth - 1, robot.getX());
    assertEquals(tableHeight, robot.getY());

    robot.setX(tableWidth);
    robot.setY(tableHeight);
    robot.setFace(Face.NORTH);
    appUnderTest.moveRobot();
    assertEquals(tableWidth, robot.getX());
    assertEquals(tableHeight, robot.getY());

    robot.setX(tableWidth);
    robot.setY(tableHeight);
    robot.setFace(Face.EAST);
    appUnderTest.moveRobot();
    assertEquals(tableWidth, robot.getX());
    assertEquals(tableHeight, robot.getY());

    robot.setX(tableWidth);
    robot.setY(tableHeight);
    robot.setFace(Face.SOUTH);
    appUnderTest.moveRobot();
    assertEquals(tableWidth, robot.getX());
    assertEquals(tableHeight - 1, robot.getY());
  }

  @Test
  public void test_moveRobot_bottomRightCorner() {
    robot.setX(tableWidth);
    robot.setY(0);
    robot.setFace(Face.WEST);
    appUnderTest.moveRobot();
    assertEquals(tableWidth - 1, robot.getX());
    assertEquals(0, robot.getY());

    robot.setX(tableWidth);
    robot.setY(0);
    robot.setFace(Face.NORTH);
    appUnderTest.moveRobot();
    assertEquals(tableWidth, robot.getX());
    assertEquals(1, robot.getY());

    robot.setX(tableWidth);
    robot.setY(0);
    robot.setFace(Face.EAST);
    appUnderTest.moveRobot();
    assertEquals(tableWidth, robot.getX());
    assertEquals(0, robot.getY());

    robot.setX(tableWidth);
    robot.setY(0);
    robot.setFace(Face.SOUTH);
    appUnderTest.moveRobot();
    assertEquals(tableWidth, robot.getX());
    assertEquals(0, robot.getY());
  }

  @Test
  public void test_moveRobot() {
    robot.setX(3);
    robot.setY(2);
    robot.setFace(Face.WEST);
    appUnderTest.moveRobot();
    assertEquals(2, robot.getX());
    assertEquals(2, robot.getY());

    robot.setX(4);
    robot.setY(4);
    robot.setFace(Face.NORTH);
    appUnderTest.moveRobot();
    assertEquals(4, robot.getX());
    assertEquals(5, robot.getY());
  }

  @Test
  public void test_executeCommand_moveLeft() {
    robot.setFace(Face.NORTH);
    appUnderTest.executeCommand("LEFT");
    assertEquals(Face.WEST, robot.getFace());

    robot.setFace(Face.WEST);
    appUnderTest.executeCommand("LEFT");
    assertEquals(Face.SOUTH, robot.getFace());

    robot.setFace(Face.SOUTH);
    appUnderTest.executeCommand("LEFT");
    assertEquals(Face.EAST, robot.getFace());

    robot.setFace(Face.EAST);
    appUnderTest.executeCommand("LEFT");
    assertEquals(Face.NORTH, robot.getFace());
  }

  @Test
  public void test_executeCommand_moveRight() {
    robot.setFace(Face.NORTH);
    appUnderTest.executeCommand("RIGHT");
    assertEquals(Face.EAST, robot.getFace());

    robot.setFace(Face.WEST);
    appUnderTest.executeCommand("RIGHT");
    assertEquals(Face.NORTH, robot.getFace());

    robot.setFace(Face.SOUTH);
    appUnderTest.executeCommand("RIGHT");
    assertEquals(Face.WEST, robot.getFace());

    robot.setFace(Face.EAST);
    appUnderTest.executeCommand("RIGHT");
    assertEquals(Face.SOUTH, robot.getFace());
  }

  @Test
  public void test_executeCommand_report() {
    robot.setX(1);
    robot.setY(2);
    robot.setFace(Face.NORTH);
    appUnderTest.executeCommand("REPORT");
    assertEquals("Output: 1,2,NORTH", robot.getInfo());
  }
}