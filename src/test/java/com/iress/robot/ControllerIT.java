package com.iress.robot;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import org.junit.Before;
import org.junit.Test;

/**
 * Controller Integration test. It reads test data from TestData.txt file and executes tests.
 *
 * Inputs from file should be in form of Command1;Command2...=Expected Output.
 *
 * @author dhwaneel
 */
public class ControllerIT {

  private Controller appUnderTest;
  private Robot robot;
  private Scanner scanner;

  @Before
  public void setUp() throws FileNotFoundException {
    ClassLoader classLoader = getClass().getClassLoader();

    scanner = new Scanner(
        new File(classLoader.getResource("TestData.txt").getFile()));

    appUnderTest = new Controller();
    robot = appUnderTest.getRobot();
  }

  @Test
  public void runTestCases() throws IOException {
    System.out.println("Reading from file and executing tests");

    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      //Input in form of Command1;Command2...=Expected Output
      String[] testData = line.split("=");

      String[] commands = testData[0].split(";");
      for (String command : commands) {
        this.appUnderTest.executeCommand(command);
      }
      assertEquals(testData[1], robot.getInfo());
    }
  }
}
