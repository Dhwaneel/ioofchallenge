package com.iress.robot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * FaceEnum test.
 *
 * @author dhwaneel
 */
public class FaceTest {

  @Test
  public void testLeft() {
    assertEquals(Face.WEST, Face.NORTH.left());
    assertEquals(Face.SOUTH, Face.NORTH.left().left());
  }

  @Test
  public void testRight() {
    assertEquals(Face.EAST, Face.NORTH.right());
    assertEquals(Face.SOUTH, Face.NORTH.right().right());
  }
}