package com.iress.robot;

/**
 * It initiates the app.
 *
 * @author dhwaneel
 */
public class App {

  public static void main(String[] args) {
    Controller controller = new Controller();
    controller.read();
  }
}
