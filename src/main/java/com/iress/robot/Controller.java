package com.iress.robot;

import java.util.Arrays;
import java.util.Scanner;

/**
 * It reads inputs and executes commands for robot to move.
 *
 * @author dhwaneel
 */
public class Controller {

  private Robot robot;
  private Table table;
  private static final int TABLE_WIDTH = 5;
  private static final int TABLE_HEIGHT = 5;


  public Controller() {
    table = new Table(TABLE_WIDTH, TABLE_HEIGHT);
    robot = new Robot();
  }

  public void read() {
    System.out.println(
        "Please enter one of the following commands: " + Arrays.asList(Command.values()));
    System.out.println("For placing a robot please use the format " + Command.PLACE + " X,Y,F");
    Scanner scanner = new Scanner(System.in);
    while (true) {
      String command = scanner.nextLine();
      if (command != null && command.contains(Command.EXIT.name())) {
        System.out.println("Exiting the App. Thanks for playing.");
        break;
      }

      if (commandValid(command)) {
        executeCommand(command);
      }
    }
  }

  void executeCommand(String command) {
    //Command be in form of 'PLACE instructions' or simply 'MOVE'
    String[] commandKeywords = command.split(" ");
    //Select command to execute
    switch (Command.valueOf(commandKeywords[0])) {
      case PLACE:
        this.placeRobot(commandKeywords[1]);
        break;
      case MOVE:
        this.moveRobot();
        break;
      case LEFT:
        this.robot.moveLeft();
        break;
      case RIGHT:
        this.robot.moveRight();
        break;
      case REPORT:
        System.out.println();
        System.out.println("----------------");
        System.out.println(this.robot.getInfo());
        System.out.println("----------------");
        break;
    }
  }

  /**
   * It moves robot by 1 unit in the direction it is facing.
   */
  void moveRobot() {
    int updatedX = robot.getX();
    int updatedY = robot.getY();

    switch (robot.getFace()) {
      case NORTH:
        updatedY += 1;
        break;
      case WEST:
        updatedX -= 1;
        break;
      case SOUTH:
        updatedY -= 1;
        break;
      case EAST:
        updatedX += 1;
        break;
      //NOTE: we could add a default case for un-matching face.
    }

    if (!robotFalling(updatedX, updatedY)) {
      robot.setX(updatedX);
      robot.setY(updatedY);
    } else {
      System.out.println("Command ignored as Robot will fall");
    }
  }

  /**
   * Places robot at the required position based on position.
   *
   * @param position the Position in form of X,Y,F
   */
  void placeRobot(String position) {
    //In form of X,Y,F
    String[] positionParams = position.split(",");
    int x = Integer.parseInt(positionParams[0]);
    int y = Integer.parseInt(positionParams[1]);
    String face = positionParams[2];

    if (!robotFalling(x, y)) {
      this.robot.setX(x);
      this.robot.setY(y);
      this.robot.setFace(Face.valueOf(face));
    } else {
      System.out.println(
          "Robot could not be placed. Please enter X,Y values between 0,0 and " + table.getWidth()
              + "," + table.getHeight());
    }
  }

  private boolean robotFalling(int x, int y) {
    return x < 0 || y < 0 || x > table.getWidth() || y > table.getHeight();
  }

  private boolean commandValid(String command) {
    if (command == null || command.isEmpty()) {
      System.out.println("Inputs cannot be null or empty");
      return false;
    }
    String[] commandKeywords = command.split(" ");
    if (commandKeywords.length == 0 || !Command.contains(commandKeywords[0])) {
      System.out.println(
          "Command invalid. Please enter one of the following commands: " + Arrays
              .asList(Command.values()));
      return false;
    }

    Command inputCommand = Command.valueOf(commandKeywords[0]);
    if (robotNotPlaced(inputCommand)) {
      System.out.println(
          "Please place the robot in an appropriate position before issuing any other command");
      return false;
    }
    //We could add more validation e.g. Format for PLACE command.
    return true;
  }

  private boolean robotNotPlaced(Command inputCommand) {
    return this.robot.getFace() == null && Command.PLACE != inputCommand;
  }

  Robot getRobot() {
    return robot;
  }

  Table getTable() {
    return table;
  }
}
