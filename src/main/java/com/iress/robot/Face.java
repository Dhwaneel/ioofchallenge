package com.iress.robot;

/**
 * It represents the Robot's Face direction.
 *
 * @author dhwaneel
 */
public enum Face {
  NORTH,
  SOUTH,
  EAST,
  WEST;

  public Face left() {
    switch (this) {
      case NORTH:
        return WEST;

      case WEST:
        return SOUTH;

      case SOUTH:
        return EAST;

      case EAST:
        return NORTH;

      default:
        return this;

    }
  }

  public Face right() {
    switch (this) {
      case NORTH:
        return EAST;

      case EAST:
        return SOUTH;

      case SOUTH:
        return WEST;

      case WEST:
        return NORTH;

      default:
        return this;

    }
  }
}
