package com.iress.robot;

/**
 * It contains list of commands used by the app.
 *
 * @author dhwaneel
 */
public enum Command {
  PLACE,
  MOVE,
  LEFT,
  RIGHT,
  REPORT,
  EXIT;

  /**
   * It checks whether commandKeyword represents a valid Command.
   *
   * @param commandKeyword the command keyword
   * @return <code>true</code> if commandKeyword is a valid command else <code>false</code>
   */
  public static boolean contains(String commandKeyword) {
    boolean validCommand = false;
    for (Command command : Command.values()) {
      if (command.name().equals(commandKeyword)) {
        validCommand = true;
        break;
      }
    }
    return validCommand;
  }
}
