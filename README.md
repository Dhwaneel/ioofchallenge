# Iress Toy Robot Challenge

This project refers to Iress code challenge. The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
The project folder refers to ``/IressChallenge``. 

## Getting Started

### Prerequisites

```
Maven 3.5.2 or later
Java 8 
```

### Compiling
Go to project folder and run

```
mvn clean install
```

### Running the app
Go to project folder and run

```
java -jar target/robot-1.0-SNAPSHOT.jar
```

## Running the tests
The project contains both unit as well as integration tests.

### Unit tests

Go to project folder and run

```
mvn test
```

### Integration tests
Test data is provided under ``IressChallenge/src/test/resources/TestData.txt``
Please add more test cases as required using the format used in file (Command1;Command2...=Expected Output).

To run tests, navigate to project folder and run

```
mvn verify
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

 **Dhwaneel Trivedi** 